#include "stdafx.h"
#include "program.h"
#include <string>
#include <iostream>
using namespace std;

namespace simple_shapes
{
	Shape* Shape::In(ifstream &in)
	{
		Shape *sp;
		int k;
		in.clear();
		if (!(in >> k))
		{
			cout << "Reading Key: Incorrect data!" << endl;
			exit(1);
		}
		switch (k)
		{
		case 1:
			sp = new Aphorism;
			break;
		case 2:
			sp = new Proverb;
			break;
		case 3:
			sp = new Riddle;
			break;
		default:
			cout << "Reading Key: Incorrect data!" << endl;
			exit(1);
		}
		string temp;
		getline(in, temp);
		sp->InData(in);
		getline(in, sp->text);
		in.clear();
		if (!(in >> sp->assessment))
		{
			cout << "Reading Assessment: Incorrect data!" << endl;
			exit(1);
		}
		if (sp->assessment > 10 || sp->assessment < 0)
		{
			cout << "Reading Assessment: Incorrect data!" << endl;
			exit(1);
		}
		return sp;
	}
	int Shape::Count()
	{
		int result = 0;
		for (int i = 0; i < text.length(); i++)
		if (text[i] == ',' || text[i] == '.' || text[i] == '?' ||
			text[i] == '!' || text[i] == '(' || text[i] == ')' ||
			text[i] == '"' || text[i] == '-' || text[i] == ':')
			result++;
		return result;
	}
	void Aphorism::InData(ifstream &in)
	{
		getline(in, author);
	}

	void Proverb::InData(ifstream &in)
	{
		getline(in, country);
	}
	void Riddle::InData(ifstream &in)
	{
		getline(in, answer);
	}
	void Aphorism::Out(ofstream &out)
	{
		out << "It is Aphorism: author = " << author << ", text = " << text  << " assessment - " << assessment << ", count of punctuation marks = " << Count() << endl;
	}

	void Proverb::Out(ofstream &out)
	{
		out << "It is Proverb: country = " << country << ", text = " << text << " assessment - " << assessment << ", count of punctuation marks = " << Count() << endl;
	}
	void Riddle::Out(ofstream &out)
	{
		out << "It is Riddle: answer = " << answer << ", text = " << text << " assessment - " << assessment << ", count of punctuation marks = " << Count() << endl;
	
	}
	void List::Clear()
	{
		while (size != 0) // ���� ����������� ������ �� ������ ������� 
		{
			Node *temp = head->next;
			delete head; // ����������� ������ �� ��������� ��������
			head = temp; // ����� ������ ������ �� ����� ���������� ��������
			size--; // ���� ������� ����������. ������������ ����� ���������
		}
	}

	void List::In(ifstream &in)
	{
		while (!in.eof())
		{
			size++; // ��� ������ ���������� �������� ����������� ����� ��������� � ������
			Node  *temp = new Node; // ��������� ������ ��� ������ �������� ������
			temp->x = Shape::In(in); // ���������� � ���������� ������ ������ �������� x 

			if (head != NULL) // � ��� ������ ���� ������ �� ������
			{
				tail->next = temp; // ������ ������ � ��������� �� ��������� ��������� ����
				temp->prev = tail;
				tail = temp; // ��������� �������� �������=������ ��� ���������.
			}
			else head = tail = temp; // ���� ������ ���� �� ��������� ������ �������.
		}
	}

	void List::Out(ofstream &out)
	{
		Node *tempHead = head; // ��������� �� ������

		int temp = size; // ��������� ���������� ������ ����� ��������� � ������
		int i = 0;
		while (temp != 0) // ���� �� �������� ������� ������� �� ����� ������
		{
			out << i << ": ";
			i++;
			tempHead->x->Out(out); // ��������� ������� ������ �� ����� 
			tempHead = tempHead->next; // ���������, ��� ����� ��������� �������
			temp--; // ���� ������� ������, ������ �������� �� ���� ������ 
		}
	}
	bool Shape::Compare(Shape &other)
	{
		return Count() < other.Count();
	}

	void List::Sort()
	{
		Node *p = head;
		for (int i = 0; i < size - 1; i++)
		{
			Node *temp = p->next;
			for (int j = i + 1; j < size; j++)
			{
				if (p->x->Compare(*temp->x))
				{
					Shape *tmp = p->x;
					p->x = temp->x;
					temp->x = tmp;
				}
				temp = temp->next;
			}
			p = p->next;
		}
	}
	void Shape::OutAphorisms(ofstream &out)
	{
		out;
	}

	void Aphorism::OutAphorisms(ofstream &out)
	{
		Out(out);
	}
	
	void List::OutAphorisms(ofstream &out)
	{
		Node * tempHead = head; // ��������� �� ������
		out << "Only Aphorisms." << endl;
		int temp = size; // ��������� ���������� ������ ����� ��������� � ������
		while (temp != 0) // ���� �� �������� ������� ������� �� ����� ������
		{		
			tempHead->x->OutAphorisms(out); // ��������� ������� ������ �� ����� 
			tempHead = tempHead->next; // ���������, ��� ����� ��������� �������
			temp--; // ���� ������� ������, ������ �������� �� ���� ������ 
		}
	}
	List::List() :head(NULL), tail(NULL), size(0) {};
	void Aphorism::MultiMethod(Shape *other, ofstream &ofst)
	{
		other->MMAphorism(ofst);
	}
	void Proverb::MultiMethod(Shape *other, ofstream &ofst)
	{
		other->MMProverb(ofst);
	}
	void Riddle::MultiMethod(Shape *other, ofstream &ofst)
	{
		other->MMRiddle(ofst);
	}
	void Proverb::MMProverb(ofstream &ofst)
	{
		ofst << "Proverb and Proverb" << endl;
	}
	void Proverb::MMAphorism(ofstream &ofst)
	{
		ofst << "Aphorism and Proverb" << endl;
	}
	void Proverb::MMRiddle(ofstream &ofst)
	{
		ofst << "Riddle and Proverb" << endl;
	}
	void Aphorism::MMProverb(ofstream &ofst)
	{
		ofst << "Proverb and Aphorism" << endl;
	}
	void Aphorism::MMAphorism(ofstream &ofst)
	{
		ofst << "Aphorism and Aphorism" << endl;
	}
	void Aphorism::MMRiddle(ofstream &ofst)
	{
		ofst << "Riddle and Aphorism" << endl;
	}
	void Riddle::MMProverb(ofstream &ofst)
	{
		ofst << "Proverb and Riddle" << endl;
	}
	void Riddle::MMRiddle(ofstream &ofst)
	{
		ofst << "Riddle and Riddle" << endl;
	}
	void Riddle::MMAphorism(ofstream &ofst)
	{
		ofst << "Aphorism and Riddle" << endl;
	}
	void List::MultiMethod(ofstream &out)
	{
		out << "Multimethod." << endl;
		Node *tempHead = head;
		for (int i = 0; i < size - 1; i++)
		{
			Node *elem = tempHead->next;
			for (int j = i + 1; j < size; j++)
			{
				tempHead->x->MultiMethod(elem->x, out);
				tempHead->x->Out(out);
				elem->x->Out(out);
				elem = elem->next;
			}
			tempHead = tempHead->next;
		}
	}
}