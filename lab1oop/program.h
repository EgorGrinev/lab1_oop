#ifndef __program__
#define __program__

#include <fstream>
using namespace std;

namespace simple_shapes
{
	class Shape
	{

	public:
		static Shape* In(ifstream &in);
		virtual void InData(ifstream &in) = 0;
		virtual void Out(ofstream &out) = 0;
		virtual void OutAphorisms(ofstream &out);
		virtual void MultiMethod(Shape *other, ofstream &ofst) = 0;
		virtual void MMAphorism(ofstream &ofst) = 0;
		virtual void MMProverb(ofstream &ofst) = 0;
		virtual void MMRiddle(ofstream &ofst) = 0;
		int Count();
		bool Compare(Shape &other);

	protected:
		Shape() {};
		string text;
		int assessment;
	};

	class Aphorism : public Shape
	{
		string author;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		void OutAphorisms(ofstream &out);
		void MultiMethod(Shape *other, ofstream &ofst);
		void MMAphorism(ofstream &ofst);
		void MMProverb(ofstream &ofst);
		void MMRiddle(ofstream &ofst);
		Aphorism() {} // �������� ��� ��������������
	};

	class Proverb : public Shape
	{
		string country;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		void MultiMethod(Shape *other, ofstream &ofst);
		void MMAphorism(ofstream &ofst);
		void MMProverb(ofstream &ofst);
		void MMRiddle(ofstream &ofst);
		Proverb() {} // �������� ��� ��������������
	};
	class Riddle : public Shape
	{
		string answer;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		void MultiMethod(Shape *other, ofstream &ofst);
		void MMAphorism(ofstream &ofst);
		void MMProverb(ofstream &ofst);
		void MMRiddle(ofstream &ofst);
		Riddle() {} // �������� ��� ��������������
	};
	class container
	{
		enum
		{
			max_len = 100 // ������������ �����
		};
		int len; // ������� �����
		Shape *cont[max_len];
	public:
		void In(ifstream &in);
		void Out(ofstream &out);
		void Clear();
		container(); // ������������� ����������
		~container()
		{
			Clear();
		}
	};
	struct  Node
	{
		Shape* x;
		Node *next;
		Node *prev;
	};
	class List
	{
		Node *head, *tail; // ������ ������� � ��� ��� ���������
		int size; // ����� ��������� � ������
	public:
		List(); // ������������� ��������� � ���� � ������� ������������

		~List() // ����������
		{
			Clear();
		};
		void Sort();
		void OutAphorisms(ofstream &out);
		void Clear(); // ���������� ��� ������������ ������
		void In(ifstream &in); // ������� ���������� ��������� � ������
		void Out(ofstream &out); // ������� ����������� ��������� ������
		void MultiMethod(ofstream &ofst);
	};
}
#endif 
